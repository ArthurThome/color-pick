﻿// Arthur
// 21 May 2020

#region Statements

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

#endregion

public class HUBManager : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private TextMeshProUGUI TMP_pigmentsCount;

    [ SerializeField ] private int pigmentsQuantity = 200;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        TMP_pigmentsCount.text = pigmentsQuantity.ToString ( );
    }

    #endregion
}
